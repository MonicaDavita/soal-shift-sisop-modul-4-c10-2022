# soal-shift-sisop-modul-4-C10-2022

Pembahasan soal praktikum Modul 4 Sistem Operasi 2022

**Anggota Kelompok**:

- Sarah Alissa Putri - 5025201272
- Monica Narda Davita - 5025201009
- William Zefanya Maranatha - 5025201167

---

## Soal 1
[Source code soal1](https://gitlab.com/MonicaDavita/soal-shift-sisop-modul-4-c10-2022/-/blob/main/anya_c10.c)

### Deskripsi Soal
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
### Soal 1.a
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13.
`contoh: Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt`

```ruby
int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
Potongan kode di atas merupakan fungsi main standar untuk membuat filesistem.

```ruby
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    char fpath[1000];
    strcpy(fpath, handlePath(path));
    int res;
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;
    return 0;
}
```
Fungsi standar untuk mendapatkan atribut dari suatu file/folder.

```ruby
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    strcpy(fpath, handlePath(path));
    int fd;
    int res;
    (void)fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;
    close(fd);
    return res;
}
```
Fungsi untuk membaca suatu file dan mengecek apakah ada error atau tidak

```ruby
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};
```
Struct operasi fuse untuk mendapatkan atribut, membaca direktori, membaca file, dan mengubah nama suatu direktori.

```ruby
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000], *p_path;
    bool animeku = strstr(path, "/Animeku_");
    strcpy(fpath, handlePath(path));
    (void)offset;
    (void)fi;
    int res = 0;
    DIR *dp;
    struct dirent *de;
    dp = opendir(fpath);
    if (dp == NULL)
        return errno;
    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
        {
            res = (filler(buf, de->d_name, &st, 0));
        }
        else if (animeku)
        {
            if (de->d_type & DT_DIR)
            {
                char temp[1000];
                strcpy(temp, de->d_name);
                res = (filler(buf, temp, &st, 0));
            }
            else
            {
                char *p_ext;
                p_ext = strrchr(de->d_name, '.');
                char fileName[1000] = "";
                if (p_ext)
                {
                    strncpy(fileName, de->d_name, strlen(de->d_name) -

                                                      strlen(p_ext));

                    strcpy(fileName, cipher(fileName));
                    strcat(fileName, p_ext);
                }
                else
                {
                    strcpy(fileName, cipher(de->d_name));
                }
                res = (filler(buf, fileName, &st, 0));
            }
        }
        if (de->d_type & DT_DIR)
        {
            char temp[1000];
            strcpy(temp, de->d_name);
            res = (filler(buf, temp, &st, 0));
        }
        else
            res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}
```
Fungsi untuk membaca path lengkap dari suatu folder berawalan "Animeku/" dan memanggil fungsi untuk mengencode serta decode nama file dari folder tersebut.

```ruby
char *cipher(char *string)
{
    char temp[1000];
    strcpy(temp, string);
    for (int i = 0; i < strlen(string); i++)
    {
        if (isupper(string[i]))
        {
            temp[i] = (char)(90 - (string[i] - 65));
        }
        else if (islower(string[i]))
        {
            if (string[i] <= 109)
                temp[i] = (char)(string[i] + 13);
            else
                temp[i] = (char)(string[i] - 13);
        }
    }
    char *result = temp;
    return result;
}
```
Fungsi untuk mengencode serta decode nama file.

### Soal 1.b
Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

### Soal 1.c
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode

```ruby
static int xmp_rename(const char *old, const char *new){
    char fPathOld[1000], fPathNew[1000], *p_fPathOld, *p_fPathNew;

    if(!strcmp(old, "/")){
        sprintf(fPathOld, "%s", dirpath);
    }
    else{
        sprintf(fPathOld, "%s%s", dirpath, old);
    }

     if(!strcmp(new, "/")){
        sprintf(fPathNew, "%s", dirpath);
    }
    else{
        sprintf(fPathNew, "%s%s", dirpath, new);
    }

    if(rename(fPathOld, fPathNew)==-1){
        return errno;
    }
    p_fPathOld = strrchr(fPathOld, '/');
    p_fPathNew = strrchr(fPathNew, '/');
    if(strstr(p_fPathOld, "Animeku_")) writeLog("terdecode", fPathOld, fPathNew);
    if(strstr(p_fPathNew, "Animeku_")) writeLog("terenkripsi", fPathOld, fPathNew);
    return 0;
}
```
Fungsi untuk encode dan decode folder dengan awalan "Animeku/" dan menulis perubahannya (path foldernya) pada Wibu.log. 

```ruby
char *handlePath(const char *path)
{
    char fpath[2000], *rfpath, *p_path, temp_p_path[1000] = "", *p_temp_p_path;
    if (!strcmp(path, "/"))
    {
        sprintf(fpath, "%s", dirpath);
    }
    else
    {
        if (strstr(path, "Animeku_"))
        {
            p_path = strrchr(path, '/');
            strncpy(temp_p_path, path, strlen(path) - strlen(p_path));
            p_temp_p_path = strrchr(temp_p_path, '/');
            if (strstr(p_path, "/Animeku_"))
            {
                char srcDirName[1000] = "";
                char *p_srcFileName = strrchr(path, '/');
                strncpy(srcDirName, path, strlen(path) - strlen(p_srcFileName));
                sprintf(fpath, "%s%s%s", dirpath, srcDirName, p_srcFileName);
            }
            else if (strstr(p_temp_p_path, "/Animeku_"))
            {
                char srcDirName[1000] = "", srcFileName[1000] = "";
                char *p_srcFileName = strrchr(path, '/');
                char *p_ext;
                strncpy(srcDirName, path, strlen(path) - strlen(p_srcFileName));
                p_ext = strrchr(p_srcFileName, '.');
                // if file has extension (.txt, .zip, ...)
                if (p_ext)
                {
                    strncpy(srcFileName, p_srcFileName, strlen(p_srcFileName) - strlen(p_ext));
                    sprintf(fpath, "%s%s%s%s", dirpath, srcDirName, cipher(srcFileName), p_ext);
                }
                else
                {
                    char testDirPath[] = "";
                    sprintf(testDirPath, "%s%s", dirpath, path);
                    DIR *dp = opendir(testDirPath);
                    if (dp)
                        sprintf(fpath, "%s%s%s", dirpath, srcDirName, p_srcFileName);
                    else
                        sprintf(fpath, "%s%s%s", dirpath, srcDirName, cipher(p_srcFileName));
                }
            }
            else
            {
                sprintf(fpath, "%s%s", dirpath, path);
            }
        }
        else
        {
            sprintf(fpath, "%s%s", dirpath, path);
        }
    }
    rfpath = fpath;
    return rfpath;
}
```
Fungsi untuk mengatur path file yang sesungguhnya (yang benar) dan meng-encode dan decode nama file yang sesuai.

### Soal 1.d
Setiap data yang terencode akan masuk dalam file “Wibu.log”
`RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba`
```ruby
void writeLog(char *message, char *old, char *new)
{
    FILE *fptr;
    fptr = fopen("Wibu.log", "a+");

    if (fptr == NULL)
    {
        printf("Error");
        exit(1);
    }

    fprintf(fptr, "RENAME\t%s\t%s\t-->\t%s\n", message, old, new);
    fclose(fptr);
}
```
Fungsi untuk menulis perubahan (rename) pada Wibu.log

### Soal 1.e
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)
```ruby
Hal ini dapat dilakukan dengan fungsi xmp_readdir pada soal 1.a di atas
```

## Kendala
Ada di masalah pribadi sehingga kesulitan mengerjakan :)

## Bukti-Bukti
- Nama file pada folder Animeku_ sebelum terenkripsi
![priorFilenameAnimeku_](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Nama_file_sebelum_terenkripsi_folder_Animeku_.jpeg)

- Bukti nama file terenkripsi pada folder Animeku_
![postFilenameEncryptAnimeku_](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Nama_file_terenkripsi_pada_folder_Animeku_.jpeg)

- Bukti log rename ke file Wibu.log (enkripsi)
![logEncryptWibu](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Bukti_rename_terenkripsi_wibu.log.jpeg)

- Nama awal file pada suatu folder yang belum direname
![beforeRenameFolder](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Nama_file_sebelum_dienkripsi_folder_rename.jpeg)

- Bukti nama file terenkripsi setelah folder direname menjadi Animeku_Modul1 
![encryptedFilenameAfterRename](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Nama_file_setelah_terenkripsi_folder_direname.jpeg)

- Rename folder Animeku_Modul1 menjadi Ahay
![renameAnimeku_Modul1ToAhay](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Rename_folder_Animeku_anya_menjadi_Ahay.jpeg)

- Bukti nama file terdecode pada folder Ahay
![decodedFilenameAfterRename](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Bukti_nama_file_terdecode_folder_Ahay.jpeg)

- Bukti log nama folder telah direname pada file Wibu.log (decode)
![logDecryptedWibu](https://gitlab.com/MonicaDavita/foto-foto/-/raw/main/Modul%204/Bukti_rename_folder_decode_ke_Wibu.log.jpeg)
